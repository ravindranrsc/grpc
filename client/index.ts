import { User, UserStatus } from "../proto/users_pb";
import getUser from "./get-user";
import createUsers from "./create-users";
import allUsers from "./all-users";

async function run() {
  const user = await getUser(1);
  console.log(user.toString());

/*   const jim = new User();
  jim.setName("Ravindran");
  jim.setAge(45);
  jim.setId(3);
  jim.setStatus(UserStatus.BUSY); 
  
  createUsers([jim]);
  console.log(`\nCreated user ${jim.toString()}`);
  */
const ul:any=[];
  const user_list = new User();
  user_list.setName("REvathi");
  user_list.setAge(45);
  user_list.setId(3);
  user_list.setStatus(UserStatus.BUSY);
ul.push(user_list);
ul.push(user_list);

  createUsers(ul);
  console.log(`\nCreated user ${ul.toString()}`);

  const users = await allUsers();
  console.log(`\nListing all ${users.length} users`);
  for (const user of users) {
    console.log(user.toString());
  }
}

run();
